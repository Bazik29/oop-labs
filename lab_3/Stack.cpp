#include "Stack.hpp"
#include <iostream>

Stack::stack* Stack::stack_push(stack* stck, char inf)
{
    stack* beg = new stack;
    beg->inf = inf;
    beg->next = stck;
    return beg;
}

char Stack::stack_pop(stack*& stck)
{
    if (!stck)
        throw "Стек пуст.";

    int pop_inf = stck->inf;
    if (stck->next) {
        stack* tempPtr = stck->next;
        delete stck;
        stck = tempPtr;
    } else {
        delete stck;
        stck = nullptr;
    }
    return pop_inf;
}

void Stack::stack_revers(stack*& stck)
{
    if (!stck)
        throw "Стек пуст.";

    stack* newStack = nullptr;
    while (stck)
        newStack = stack_push(newStack, stack_pop(stck));
    stck = newStack;
}

void Stack::stack_print(stack*& stck)
{
    if (!stck)
        throw "Стек пуст.";

    stack_revers(stck);

    stack* newStack = nullptr;
    while (stck) {
        int tempInf = stack_pop(stck);
        std::cout << tempInf << std::endl;
        newStack = stack_push(newStack, tempInf);
    }
    stck = newStack;
}

void Stack::push(char inf)
{
    stack_head = stack_push(stack_head, inf);
}

char Stack::pop()
{
    char pop_inf = stack_pop(stack_head);
    return pop_inf;
}

bool Stack::isEmpty()
{
    return stack_head == nullptr;
}

void Stack::revers()
{
    stack_revers(stack_head);
}

void Stack::print()
{
    stack_print(stack_head);
}
