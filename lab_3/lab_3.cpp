#include "Stack.hpp"

#include <iostream>
#include <optional>
#include <string>

void checkString(std::string str)
{
    Stack stack;
    int len = str.size();

    char pop_char;

    for (int i = 0; i < len; i++) {
        if (str[i] == '(') {
            stack.push('(');
            continue;
        }
        if (str[i] == '[') {
            stack.push('[');
            continue;
        }
        if (str[i] == '{') {
            stack.push('{');
            continue;
        }

        if (str[i] == ')') {
            if (!stack) {
                std::cout << "Нет открывающей скобки '('\n";
                return;
            }
            pop_char = stack.pop();
            if (pop_char != '(') {
                std::cout << "Неверное закрытие скобкой ')'\n"
                          << str.substr(0, i + 1) << "\n";
                return;
            }
            continue;
        }
        if (str[i] == ']') {
            if (!stack) {
                std::cout << "Нет открывающей скобки '['\n";
                return;
            }
            pop_char = stack.pop();
            if (pop_char != '[') {
                std::cout << "Неверное закрытие скобкой ']'\n"
                          << str.substr(0, i + 1) << "\n";
                return;
            }
            continue;
        }
        if (str[i] == '}') {
            if (!stack) {
                std::cout << "Нет открывающей скобки '{'\n";
                return;
            }
            pop_char = stack.pop();
            if (pop_char != '{') {
                std::cout << "Неверное закрытие скобкой '}'\n"
                          << str.substr(0, i + 1) << "\n";
                return;
            }
            continue;
        }
    }

    if (stack) {
        pop_char = stack.pop();
        std::cout << "Нет открывающей скобки '" << pop_char << "'\n";
        std::cout << str << "'\n";
        return;
    }

    std::cout << "Тест пройден!\n";
}

int main(int argc, char const* argv[])
{

    std::string str_1 = "()[{}]";
    std::string str_2 = "[()()] [({}])";
    std::string str_3 = "[( )( )] [{ }] ]";

    checkString(str_1);
    checkString(str_2);
    checkString(str_3);

    return 0;
}
