#pragma once

#include <cstddef>
#include <optional>

class Stack {
    struct stack {
        char inf;
        stack* next;
    };
    stack* stack_push(stack* stck, char inf);
    char stack_pop(stack*& stck);
    void stack_revers(stack*& stck);
    void stack_print(stack*& stck);

    stack* stack_head = nullptr;

public:
    void push(char inf);
    char pop();
    void revers();
    void print();

    bool isEmpty();

    constexpr explicit operator bool() const noexcept
    {
        return stack_head != nullptr;
    };
};
