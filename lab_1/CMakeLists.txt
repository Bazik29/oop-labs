cmake_minimum_required(VERSION 3.8)

project(lab_1 LANGUAGES CXX)

add_library(Book STATIC "Book.hpp" "Book.cpp")
target_include_directories(Book PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

add_executable(${PROJECT_NAME} "lab_1.cpp")
target_link_libraries(${PROJECT_NAME} Book)
