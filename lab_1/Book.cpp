#include "Book.hpp"
#include <iostream>
using namespace std;

Book::Book() {}

Book::Book(std::string author, std::string name, std::string publishing, short year, unsigned int pages)
    : author(author)
    , name(name)
    , publishing(publishing)
    , year(year)
    , pages(pages)
{
}

Book::~Book() {}

void Book::show()
{
    cout << "Автор: " << author << "\n";
    cout << "Название: " << name << "\n";
    cout << "Издательство: " << publishing << "\n";
    cout << "Год: " << year << "\n";
    cout << "Количество страниц: " << pages << "\n";
}

std::string Book::getAuthor()
{
    return this->author;
}
void Book::setAuthor(std::string author)
{
    this->author = author;
}

std::string Book::getName()
{
    return this->name;
}
void Book::setName(std::string name)
{
    this->name = name;
}

std::string Book::getPublishing()
{
    return this->publishing;
}
void Book::setPublishing(std::string publishing)
{
    this->publishing = publishing;
}

short Book::getYear()
{
    return this->year;
}
void Book::setYear(short year)
{
    this->year = year;
}
