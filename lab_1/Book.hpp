#pragma once

#include <string>

class Book {
    std::string author;
    std::string name;
    std::string publishing;
    short year;
    unsigned int pages;

public:
    Book();
    Book(std::string author, std::string name, std::string publishing, short year, unsigned int pages);
    ~Book();

    void show();

    std::string getAuthor();
    void setAuthor(std::string author);

    std::string getName();
    void setName(std::string name);

    std::string getPublishing();
    void setPublishing(std::string publishing);

    short getYear();
    void setYear(short year);
};
