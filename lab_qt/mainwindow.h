#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void ConnectButtons();
    void ReadTemp();
public slots:
    void Result();
    void Plus();
    void Minus();
    void Multiplicate();
    void Division();
    void Sqrt();
    void MyOperate();
    void ClearLine();

    void set_0();
    void set_1();
    void set_2();
    void set_3();
    void set_4();
    void set_5();
    void set_6();
    void set_7();
    void set_8();
    void set_9();
    void set_point();
private:
    Ui::MainWindow *ui;

    double Temp;
    enum {plus,minus,muplic,div, MyOper} Operate;
};




#endif // MAINWINDOW_H
