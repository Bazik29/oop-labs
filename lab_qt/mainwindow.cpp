#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ConnectButtons();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ReadTemp()
{
    Temp = ui->InLine->text().toFloat();
    ui->InLine->clear();
    ui->InLine->setFocus();
}
void MainWindow::ClearLine()
{
    ui->InLine->clear();
    ui->InLine->setFocus();
}

void MainWindow::Plus()
{
    Operate = plus;
    ReadTemp();
}
void MainWindow::Minus()
{
    Operate = minus;
    ReadTemp();
}
void MainWindow::Multiplicate()
{
    Operate = muplic;
    ReadTemp();
}
void MainWindow::Division()
{
    Operate = div;
    ReadTemp();
}
void MainWindow::MyOperate()
{
    Operate = MyOper;
    ReadTemp();
}
void MainWindow::Sqrt()
{
    ReadTemp();
    QString result;
    if (Temp>=0){
        result = QString::number(sqrt(Temp));
    }
    else  result = ("Error!");
    ui->InLine->setText(result);
}

void MainWindow::Result()
{
    float B = ui->InLine->text().toFloat();
    ui->InLine->clear();
    QString result;
    switch (Operate) {
    case plus:
        Temp=(Temp+B);
        result = QString::number(Temp);
        break;
    case minus:
        Temp=(Temp-B);
        result = QString::number(Temp);
        break;
    case muplic:
        Temp=(Temp*B);
        result = QString::number(Temp);
        break;
    case div:
        if (B !=0 ) {Temp = (Temp/B); result = QString::number(Temp);}
        else result = "Error! (Div by zero)";
        break;
    case MyOper:
        //Вычисление остатка от деления
        if (B !=0 )
        {
            int a=Temp/B;
            Temp = Temp-a*B;
            result = QString::number(Temp);
        }
        else
            result = "Error! (Div by zero)";
        break;
    default:
        break;
    }
    ui->InLine->setText(result);
    ui->InLine->setFocus();
}


void MainWindow::set_0()
{
    ui->InLine->setText(ui->InLine->text()+"0");
    ui->InLine->setFocus();
}
void MainWindow::set_1()
{
    ui->InLine->setText(ui->InLine->text()+"1");
    ui->InLine->setFocus();
}
void MainWindow::set_2()
{
    ui->InLine->setText(ui->InLine->text()+"2");
    ui->InLine->setFocus();
}
void MainWindow::set_3()
{
    ui->InLine->setText(ui->InLine->text()+"3");
    ui->InLine->setFocus();
}
void MainWindow::set_4()
{
    ui->InLine->setText(ui->InLine->text()+"4");
    ui->InLine->setFocus();
}
void MainWindow::set_5()
{
    ui->InLine->setText(ui->InLine->text()+"5");
    ui->InLine->setFocus();
}
void MainWindow::set_6()
{
    ui->InLine->setText(ui->InLine->text()+"6");
    ui->InLine->setFocus();
}
void MainWindow::set_7()
{
    ui->InLine->setText(ui->InLine->text()+"7");
    ui->InLine->setFocus();
}
void MainWindow::set_8()
{
    ui->InLine->setText(ui->InLine->text()+"8");
    ui->InLine->setFocus();
}
void MainWindow::set_9()
{
    ui->InLine->setText(ui->InLine->text()+"9");
    ui->InLine->setFocus();
}
void MainWindow::set_point()
{
    ui->InLine->setText(ui->InLine->text()+".");
    ui->InLine->setFocus();
}

void MainWindow::ConnectButtons()
{
    ui->InLine->setFocus();
    connect(ui->But_SQRT,SIGNAL(clicked()),this,SLOT(Sqrt()));
    connect(ui->But_CE,SIGNAL(clicked()),this,SLOT(ClearLine()));
    connect(ui->But_point,SIGNAL(clicked()),this,SLOT(set_point()));
    connect(ui->But_0,SIGNAL(clicked()),this,SLOT(set_0()));
    connect(ui->But_1,SIGNAL(clicked()),this,SLOT(set_1()));
    connect(ui->But_2,SIGNAL(clicked()),this,SLOT(set_2()));
    connect(ui->But_3,SIGNAL(clicked()),this,SLOT(set_3()));
    connect(ui->But_4,SIGNAL(clicked()),this,SLOT(set_4()));
    connect(ui->But_5,SIGNAL(clicked()),this,SLOT(set_5()));
    connect(ui->But_6,SIGNAL(clicked()),this,SLOT(set_6()));
    connect(ui->But_7,SIGNAL(clicked()),this,SLOT(set_7()));
    connect(ui->But_8,SIGNAL(clicked()),this,SLOT(set_8()));
    connect(ui->But_9,SIGNAL(clicked()),this,SLOT(set_9()));
    connect(ui->MyOperate,SIGNAL(clicked()),this,SLOT(MyOperate()));
    connect(ui->MultiplicBut,SIGNAL(clicked()),this,SLOT(Multiplicate()));
    connect(ui->DivisionBut,SIGNAL(clicked()),this,SLOT(Division()));
    connect(ui->MinusBut,SIGNAL(clicked()),this,SLOT(Minus()));
    connect(ui->PlusBut,SIGNAL(clicked()),this,SLOT(Plus()));
    connect(ui->ResultBut,SIGNAL(clicked()),this,SLOT(Result()));
}
