#include <iostream>

#include "MatrixT.hpp"
#include <random>
#include <vector>

std::vector<int> randVectorInt(size_t size)
{
    size = size * size;
    std::vector<int> result(size);
    {
        std::random_device rd;
        std::mt19937_64 gen(rd());
        std::uniform_int_distribution<> dis(0, 9);

        for (size_t i = 0; i < size; i++)
            result[i] = dis(gen);
    }
    return result;
}

std::vector<double> randVectorDouble(size_t size)
{
    size = size * size;
    std::vector<double> result(size);

    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_real_distribution<double> dis(0.0, 9.0);

    for (size_t i = 0; i < size; i++)
        result[i] = dis(gen);

    return result;
}

template <typename Type>
std::vector<Type> eyeMatrix(size_t size)
{
    std::vector<double> result(size * size);
    for (size_t i = 0; i < size; i++)
        result[i * size + i] = 1;

    return result;
}

int main(int argc, const char* argv[])
{
    Matrix<double> A(4, randVectorDouble(4));
    Matrix<int> B(4, randVectorInt(4));

    std::cout << "Матрица<double>: \n" << A.toString();
    std::cout << "Матрица<int>: \n" << B.toString();

    return 0;
}
