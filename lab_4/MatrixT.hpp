#include <string>
#include <vector>

template <typename Type>
class Matrix {
public:
    Matrix(size_t size)
        : m_size(size)
        , m_data(size * size)
    {
    }

    Matrix(size_t size, std::vector<Type>&& data)
        : m_size(size)
        , m_data(data)
    {
    }

    Matrix(const Matrix& first)
        : m_size(first.m_size)
        , m_data(first.m_data)
    {
    }

    inline size_t size() const { return m_size; }
    Type norma();
    std::string toString();

    inline Type& operator()(size_t row, size_t col) { return m_data[row * m_size + col]; }
    inline const Type& operator()(size_t row, size_t col) const { return m_data[row * m_size + col]; }

    Matrix operator+(const Matrix& matrix);
    Matrix operator-(const Matrix& matrix);
    Matrix operator*(const Matrix& matrix);

private:
    size_t m_size;
    std::vector<Type> m_data;
};

#include <sstream>
#include <stdexcept>


template<typename Type>
Type Matrix<Type>::norma()
{
    Type max = 0.0;
    for (size_t i = 0; i < this->m_size; i++) {
        Type s = 0.0;
        for (size_t j = 0; j < this->m_size; j++) {
            s += std::abs((*this)(j, i));
        }
        if (s > max)
            max = s;
    }
    return max;
}

template<typename Type>
std::string Matrix<Type>::toString()
{
    std::stringstream ss;
    for (size_t i = 0; i < this->m_size; i++) {
        for (size_t j = 0; j < this->m_size; j++) {
            ss << (*this)(i, j);
            if (j != this->m_size - 1)
                ss << " ";
        }
        ss << std::endl;
    }
    return ss.str();
}

template<typename Type>
Matrix<Type> Matrix<Type>::operator+ (const Matrix<Type>& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < this->m_size; i++)
            for (size_t j = 0; j < this->m_size; j++)
                result(i, j) = (*this)(i, j) + matrix(i, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}

template<typename Type>
Matrix<Type> Matrix<Type>::operator-(const Matrix<Type>& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < this->m_size; i++)
            for (size_t j = 0; j < this->m_size; j++)
                result(i, j) = (*this)(i, j) - matrix(i, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}

template<typename Type>
Matrix<Type> Matrix<Type>::operator*(const Matrix<Type>& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < matrix.m_size; i++)
            for (size_t j = 0; j < matrix.m_size; j++)
                for (size_t k = 0; k < matrix.m_size; k++)
                    result(i, j) += (*this)(i, k) * matrix(k, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}
