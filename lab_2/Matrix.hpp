#include <string>
#include <vector>

class Matrix {
public:
    Matrix(size_t size)
        : m_size(size)
        , m_data(size * size)
    {
    }

    Matrix(size_t size, std::vector<double>&& data)
        : m_size(size)
        , m_data(data)
    {
    }

    Matrix(const Matrix& first)
        : m_size(first.m_size)
        , m_data(first.m_data)
    {
    }

    inline size_t size() const { return m_size; }

    inline double& operator()(size_t row, size_t col) { return m_data[row * m_size + col]; }
    inline const double& operator()(size_t row, size_t col) const { return m_data[row * m_size + col]; }

    Matrix operator+(const Matrix& matrix);
    Matrix operator-(const Matrix& matrix);
    Matrix operator*(const Matrix& matrix);

    double norma();

    std::string toString();

private:
    size_t m_size;
    std::vector<double> m_data;
};
