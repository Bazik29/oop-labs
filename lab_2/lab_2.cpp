#include <iostream>

#include "Matrix.hpp"
#include <random>
#include <vector>

std::vector<double> randVector(size_t size)
{
    size = size * size;
    std::vector<double> result(size);
    {
        std::random_device rd;
        std::mt19937_64 gen(rd());
        std::uniform_int_distribution<> dis(0, 9);
        for (size_t i = 0; i < size; i++)
            result[i] = dis(gen);
    }
    return result;
}

std::vector<double> eyeMatrix(size_t size)
{
    std::vector<double> result(size * size);
    for (size_t i = 0; i < size; i++)
        result[i * size + i] = 1;

    return result;
}

int main(int argc, const char* argv[])
{

    std::vector<Matrix> matrices;

    matrices.push_back(Matrix(4, randVector(4)));
    matrices.push_back(Matrix(4, randVector(4)));
    matrices.push_back(Matrix(4, eyeMatrix(4)));

    for (Matrix& mat : matrices) {
        std::cout << "Матрица: \n"
                  << mat.toString();
        std::cout << "Норма матрицы: " << mat.norma() << "\n";
        mat = mat * mat;
        std::cout << "Матрица в квадрате: \n"
                  << mat.toString();
    }

    return 0;
}
