#include "Matrix.hpp"

#include <sstream>
#include <stdexcept>

double Matrix::norma()
{
    double max = 0.0;
    for (size_t i = 0; i < this->m_size; i++) {
        double s = 0.0;
        for (size_t j = 0; j < this->m_size; j++) {
            s += std::abs((*this)(j, i));
        }
        if (s > max)
            max = s;
    }
    return max;
}

std::string Matrix::toString()
{
    std::stringstream ss;
    for (size_t i = 0; i < this->m_size; i++) {
        for (size_t j = 0; j < this->m_size; j++) {
            ss << (*this)(i, j);
            if (j != this->m_size - 1)
                ss << " ";
        }
        ss << std::endl;
    }
    return ss.str();
}

Matrix Matrix::operator+(const Matrix& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < this->m_size; i++)
            for (size_t j = 0; j < this->m_size; j++)
                result(i, j) = (*this)(i, j) + matrix(i, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}

Matrix Matrix::operator-(const Matrix& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < this->m_size; i++)
            for (size_t j = 0; j < this->m_size; j++)
                result(i, j) = (*this)(i, j) - matrix(i, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}

Matrix Matrix::operator*(const Matrix& matrix)
{
    Matrix result(this->m_size);

    if (this->m_size == matrix.m_size)
        for (size_t i = 0; i < matrix.m_size; i++)
            for (size_t j = 0; j < matrix.m_size; j++)
                for (size_t k = 0; k < matrix.m_size; k++)
                    result(i, j) += (*this)(i, k) * matrix(k, j);
    else
        throw std::invalid_argument("Wrong dimensions");

    return result;
}
